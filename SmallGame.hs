module SmallGame where

data Role = Role
    { name :: String
    , health :: Int
    , attack :: Int
    , defense :: Int
    , roleKind :: RoleKind
    } deriving Show

data RoleKind
    = Vampire
    | Zombie
    | Alien
    | Player
    deriving (Eq, Show)

data Level
    = Easy
    | Hard

data Loot
    = Pill
    | Potion
    | Sword
    | Dagger
    | Shell

data Action = Action
    { monsterId :: String}

data State
    = State
    {player :: Role
    , monsters :: [Role]}
    | Win
    | Loss
    deriving Show

affectStats :: Loot -> Role -> Role
affectStats Pill role@(Role _ h _ _ _) = role {health = h + 50}
affectStats Potion role@(Role _ h _ _ _) = role {health = h + 100}
affectStats Sword role@(Role _ _ a _ _) = role {attack = a + 10}
affectStats Dagger role@(Role _ _ a _ _) = role {attack = a + 5}
affectStats Shell role@(Role _ _ _ d _) = role {defense = d + 50}

startGame :: String -> Level -> State
startGame name Easy = State (Role name 100 50 0 Player)
 [ (Role "Vasile" 150 15 0 Vampire)
 , (Role "Bloody" 100 5 0 Zombie)
 ]
startGame name Hard = State (Role name 100 50 0 Player)
 [ (Role "Vasile" 150 15 0 Vampire)
 , (Role "Bloody" 100 5 0 Zombie)
 , (Role "Dorian" 200 15 0 Vampire)
 , (Role "Crazy" 80 8 0 Zombie )
 , (Role "Stranger" 200 20 100 Alien)
 , (Role "Monster" 300 20 100 Alien)
 ]

gloriousBattle :: State -> [State]
gloriousBattle state@(State _ monsters) = state : (gloriousBattle
 $ nextRound (Action (name $ head monsters)) state)
gloriousBattle Win = [Win]
gloriousBattle Loss = [Loss]

nextRound :: Action -> State -> State
nextRound (Action id) (State player monsters) =
  makeState foughtPlayer foughtMonster monsters
    where monster = head (filter ((== id) . name) monsters)
          foughtMonster = fight player monster
          foughtPlayer = fight foughtMonster player

makeState :: Role -> Role -> [Role] -> State
makeState (Role _ 0 _ _ Player) _ _ = Loss
makeState player monster@(Role id 0 _ _ _) monsters =
    if null newMonsters
    then Win
    else (State (getLoot monster player) newMonsters)
      where newMonsters = filter ((/= id) . name) monsters
makeState player monster monsters = State player $ update monster monsters


fight :: Role -> Role -> Role
fight (Role _ 0 _ _ _) role = role
fight (Role _ _ attack1 _ _) role@(Role _ health2 _ defense2 _)
  = role { health = max 0 (health2 - attack1 + (max 0 (defense2 `div` 2)))
         , defense = max 0 (defense2 - attack1)
         }

update :: Role -> [Role] -> [Role]
update (Role _ _ _ _ Player) _ = undefined
update r (x:xs) | name x == name r = r:xs
update r (x:xs) = x:(update r xs)

getLoot :: Role -> Role -> Role
getLoot _ role | roleKind role /= Player = undefined
getLoot (Role _ 0 _ _ kind) role = foldr affectStats role (getLootByRoleKind kind)
getLoot _ role = role

getLootByRoleKind :: RoleKind -> [Loot]
getLootByRoleKind Vampire = [Sword]
getLootByRoleKind Zombie = [Potion]
getLootByRoleKind Alien = [ Pill
                          , Dagger
                          , Shell
                          ]