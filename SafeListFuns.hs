module SafeListFuns where

import qualified Data.Either.Extra as E hiding (Either.Extra)

safeTail :: [a] -> Either String [a]
safeTail [] = Left "Could not get tail, empty list."
safeTail list = Right $ tail list

safeInit :: [a] -> Either String [a]
safeInit [] = Left "Could not get init, empty list."
safeInit list = Right $ init list

strip :: [a] -> Either String [a]
strip x = case safeInit x of
    Right t     -> safeTail t
    l@otherwise -> l