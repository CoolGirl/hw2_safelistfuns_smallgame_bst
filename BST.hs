{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE ViewPatterns  #-}

module BST where

import           Data.Char (isSpace)
import           Data.List (maximum)

data Tree a = Leaf | Node a (Tree a) (Tree a)
    deriving (Functor, Show)

find :: (Ord a) => Tree a -> a -> Bool
find Leaf _ = False
find (Node value left right) x = case compare x value of
    EQ -> True
    LT -> find left x
    GT -> find right x

insert :: (Ord a) => Tree a -> a -> Tree a
insert Leaf x = Node x Leaf Leaf
insert (Node value left right) x = case compare x value of
    LT        -> Node value (insert left x) right
    otherwise -> Node value left (insert right x)

delete :: (Ord a) => Tree a -> a -> Tree a
delete Leaf x = Leaf
delete (Node value left right) x = case compare x value of
    LT -> Node value (delete left x) right
    GT -> Node value left (delete right x)
    EQ -> case (left, right) of
        (Leaf, _)  -> right
        (_, Leaf)  -> left
        otherwise  -> let min = findMin right in Node min left $ delete right min

findMin :: (Ord a) => Tree a -> a
findMin Leaf = undefined
findMin (Node value Leaf _) = value
findMin (Node _ left _) = findMin left

toList :: (Ord a) => Tree a -> [a]
toList Leaf = []
toList (Node value left right) = toList left ++ value:(toList right)

fromList :: (Ord a) => [a] -> Tree a
fromList = foldr (flip insert) Leaf












verticalPrint :: Show a => Tree a -> String
verticalPrint = unlines . rowPrinter . fmap show

type TreeRows = [String]

rowPrinter :: Tree String -> TreeRows
rowPrinter Leaf                  = []
rowPrinter (Node key Leaf  Leaf) = [key]
rowPrinter (Node key left  Leaf) = connectOneChild key left
rowPrinter (Node key Leaf right) = connectOneChild key right
rowPrinter (Node key left right) =
    let lr@(ltop:_)  = rowPrinter left
        rr@(rtop:_)  = rowPrinter right

        ledgePos     = labelMidPosition ltop
        redgePos     = labelMidPosition rtop

        leftWidth    = 1 + maximum (map length lr)
        connectorLen = leftWidth + redgePos - 1 - ledgePos
        connector    = nspaces (ledgePos + 1) ++ replicate connectorLen '-'

        leftSubTree  = upEdge ledgePos : lr
        rightSubTree = upEdge redgePos : rr
        childrenRows = mergeChildren leftWidth leftSubTree rightSubTree
    in attachRows key (connector:childrenRows)

connectOneChild :: String -> Tree String -> TreeRows
connectOneChild label (rowPrinter -> rows) = attachRows label rows

attachRows :: String -> TreeRows -> TreeRows
attachRows label subTree@(top:_) =
    let labelMid    = labelMidPosition label
        topLabelMid = labelMidPosition top
        shortEdge   = upEdge topLabelMid
        subTreeRows = shortEdge : subTree
        padding     = abs (topLabelMid - labelMid)
        (cur, tree) = if topLabelMid < labelMid
                      then (label, map (moveRight padding) subTreeRows)
                      else (moveRight padding label, subTreeRows)
    in cur : tree
attachRows _ _ = error "Algorithm error: attach call on empty subtree"

-----------------------------------------
{- Helpers and other utility functions -}
-----------------------------------------

middle :: Int -> Int
middle x = x `div` 2

labelMidPosition :: String -> Int
labelMidPosition label =
    let (spaces, value) = span isSpace label
        valueMid        = middle $ length value
    in length spaces + valueMid

nspaces :: Int -> String
nspaces n = replicate n ' '

upEdge :: Int -> String
upEdge padding = nspaces padding ++ "|"

moveRight :: Int -> String -> String
moveRight n = (nspaces n ++)

fillRight :: Int -> String -> String
fillRight len s = s ++ nspaces (len - length s)

mergeChildren :: Int -> TreeRows -> TreeRows -> TreeRows
mergeChildren lWidth = scanDown
  where
    scanDown :: TreeRows -> TreeRows -> TreeRows
    scanDown    []     []  = []
    scanDown     l     []  = l
    scanDown    []  (r:rs) = (nspaces   lWidth   ++ r) : scanDown [] rs
    scanDown (l:ls) (r:rs) = (fillRight lWidth l ++ r) : scanDown ls rs